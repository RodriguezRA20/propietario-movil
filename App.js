// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow strict-local
//  */

// import React from 'react';
// import {
//   SafeAreaView,
//   StyleSheet,
//   ScrollView,
//   View,
//   Text,
//   StatusBar,
// } from 'react-native';

// import {
//   Header,
//   LearnMoreLinks,
//   Colors,
//   DebugInstructions,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';
// import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

// const styles = StyleSheet.create({
//   container: {
//     ...StyleSheet.absoluteFillObject,
//     height: 600,
//     width: 400,
//     justifyContent: 'flex-end',
//     alignItems: 'center',
//   },
//   map: {
//     ...StyleSheet.absoluteFillObject,
//   },
//  });

// const App: () => React$Node = () => {
//   return (
//     <View style={styles.container}>
//      <MapView
//        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
//        style={styles.map}
//        region={{
//          latitude: 37.78825,
//          longitude: -122.4324,
//          latitudeDelta: 0.015,
//          longitudeDelta: 0.0121,
//        }}
//      >
//      </MapView>
//    </View>
//   );
// };


// export default App;

// //MAP

import 'react-native-gesture-handler';

import Login from './View/Login';

import ProcesoTab from './View/componentes/ProcesoTab';
import Registro from './View/componentes/Registro';
import ContratoCliente from '../src/View/ContratoCliente'
import Contrato from './View/Contrato'; 

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Mapa from './View/componentes/Mapa';

const MainNavigator = createStackNavigator(
  {
    Home: {
      screen: Login, //Login ContratoCliente
      navigationOptions: {
        header: null
      }
    },
    Registrate: {
      screen: Registro,
      navigationOptions: {
        header: null
      }
    },
    Menu: {
      screen: ProcesoTab,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: "Home"
  }
);

const App = createAppContainer(MainNavigator);

export default App;
