import React from "react";
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableHighlight,
  AsyncStorage
} from "react-native";
import t from "tcomb-form-native";

var Form = t.form.Form;
const tipoContrato = t.enums({
  contrato1: "Alquiler",
  contrato2: "Anticretico",
  contrato3: "Venta"
});
// here we are: define your domain model
var Person = t.struct({
  // a required string
  carnet: t.String, // an optional string
  fechainicio: t.Date,
  fechafin: t.Date,
  monto: t.String,
  direccion: t.String, //requiere de una cadena
  tipo: tipoContrato
});

var options = {
  fields: {
    nombre: {
      placeholder: "Ingresa su Nombre Completo",
      error: "Tu no has registrado tu nombre"
    },
    fechainicio: {
      label: "fecha inicio de contrato",
      mode: "date", // muestra el campo Date como DatePickerAndroid
      dialogMode: "date" //modo de dialogo
    },
    fechafin: {
      label: "fecha fin de contrato",
      mode: "date", // muestra el campo Date como DatePickerAndroid
      dialogMode: "date" //modo de dialogo
    }
  }
}; // optional rendering options (see documentation)
type State = {};
class ContratoCliente extends React.Component<*, State> {
  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);
  }
  limpiarEdit() {
    this.setState({ value: null });
  }

  async onPress() {
    // call getValue() to get the values of the form
    var value = this.refs.form.getValue();
    if (value) {
      // if validation fails, value will be null
      console.log("PERSONA ===> ", value); // value here is an instance of Person
    }
    const id = await AsyncStorage.getItem("idarrendador");
    let body = {
      carnet: value.carnet,
      FechaFinal: '2019-10-10',
      FechaInicio: '2019-09-09',
      Monto: value.monto,
      Tipo: value.tipo,
      Arrend: id,
      Postula: this.props.navigation.state.params.id
    };

    console.log("BODY ====<> ", body)
    fetch("https://findyourhousesw1.000webhostapp.com/WS/reg_contratos.php", {
      //   fetch("http://192.168.0.11:8080/FindYourHouse/WS/registrar_usuario.php", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    })
      .then(response => response.json())
      .then(resp => {
        if (resp.ok) {
          alert("ContratoCliente Satisfactorio");
          //this.props.navigation.navigate("Menu");
        } else {
          console.log("LLEGO ==> ", resp.ok);
          alert("Error Ocurrio un problema al registrar");
        }
      })
      .catch(error => {
        console.log("ERROR ==> ", error);
        alert("Error no se pudo conectar con el servidor");
      });
  }
  componentDidMount() {
    console.log("============ ", this.props.navigation.state.params);
  }

  render() {
    return (
      <View style={styles.container}>
        {/* display */}
        <StatusBar backgroundColor="#003300" barStyle="light-content" />
        <Form ref="form" type={Person} options={options} />
        <TouchableHighlight
          style={styles.button}
          onPress={this.onPress}
          underlayColor="#99d9f4"
        >
          <Text style={styles.buttonText}>Realizar Contrato</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    marginTop: 5,
    padding: 20,
    backgroundColor: "#ffffff"
  },
  buttonText: {
    fontSize: 18,
    color: "white",
    alignSelf: "center"
  },
  button: {
    height: 36,
    backgroundColor: "#003300",
    borderColor: "#003300",
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: "stretch",
    justifyContent: "center"
  }
});

export default ContratoCliente;
