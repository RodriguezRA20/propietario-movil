import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  AsyncStorage
} from "react-native";
import { Card, Icon } from "react-native-elements";

export default class Profile extends Component {
  async _textouser() {
    const NameUser = await AsyncStorage.getItem("Nombre");
    console.warn("=====",NameUser)
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}></View>
        <Image
          style={styles.avatar}
          source={{
            uri:
              "https://scontent.fsrz1-2.fna.fbcdn.net/v/t1.0-9/s960x960/73241822_2748627965168592_7968976737264467968_o.jpg?_nc_cat=103&_nc_ohc=QdnhvhaC9SoAQkGBLjFwl5oPuzlGT62Bnd9DochG-UqSbefVUSuRaHAnQ&_nc_ht=scontent.fsrz1-2.fna&oh=5a6108e595566a370ec2d71114c23689&oe=5E99272F"
          }}
        />
        <View style={styles.body}>
          <View style={styles.bodyContent}>
            <Text  style={styles.name}> hola</Text>


            <Text style={styles.info}>UX Designer / Mobile developer</Text>
            <Text style={styles.description}>
               despues de todo tendremos un poco de tranbjo
               en cuanto a las medidas de indignacion
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: "#6E8BA7",
    height: 150
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
    alignSelf: "center",
    position: "absolute",
    marginTop: 80
  },
  name: {
    fontSize: 22,
    color: "#FFFFFF",
    fontWeight: "600"
  },
  body: {
    marginTop: 40
  },
  bodyContent: {
    flex: 1,
    alignItems: "center",
    padding: 30
  },
  name: {
    fontSize: 28,
    color: "#696969",
    fontWeight: "600"
  },
  info: {
    fontSize: 16,
    color: "#00BFFF",
    marginTop: 10
  },
  description: {
    fontSize: 16,
    color: "#696969",
    marginTop: 10,
    textAlign: "center"
  },
  buttonContainer: {
    marginTop: 10,
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
    backgroundColor: "#00BFFF"
  }
});
