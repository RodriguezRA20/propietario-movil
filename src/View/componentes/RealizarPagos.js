import React, { Component } from 'react';
import { View, StyleSheet, Text, Alert, TouchableHighlight, Modal } from "react-native";
//import Modal from "react-native-modalbox";
import { Card, Button, Divider, Image } from "react-native-elements";

const data = [
  {
    'nombre': 'alex',
    'monto': 200
  },
  {
    'nombre': 'alex2',
    'monto': 300
  },
  {
    'nombre': 'alex3',
    'monto': 400
  }
];
export default class RealizarPago extends Component{
  
  constructor() {
    super();
    this.state = {
      isOpen: false,
      isDisabled: false,
      swipeToClose: true,
      sliderValue: 0.3,
      pagos: [],
      modalVisible: false,
      nombre: '',
      fecha: '',
      monto: 0
    };
  }
    _showAlert = () => {
   this.refs.modal4.close();
      Alert.alert(
      'EL pago se ha realizado correctamente',
      'Gracias por su preferencia',
      [
        {text: 'OK', onPress: () =>{() => {
        this.setState({isOpen: false})
    } }},
      ],
    )
  }

  getPagos() {

    fetch('http://findyourhousesw1.000webhostapp.com/WS/obtener_pagos.php')
    .then(res => res.json())
    .then((resp) => {
      this.setState({
        pagos: resp.pagos
      })
    })
    .catch((error) => {
      console.log("Error => ", error);
    })
  }

  componentDidMount() {
    this.getPagos();
  }
    render() {
      return (
        <View style={{ flex: 1 }}>
          <Modal 
            animationType = {"slide"} 
            visible = {this.state.modalVisible}
            transparent={true}
            onRequestClose = {() => { console.log("Modal has been closed.") } }>
            <View style={{
              width: '100%',
              height: '100%',
              backgroundColor: 'rgba(52, 52, 52, 0.8)',
              borderRadius: 5,
              justifyContent: 'center', 
              alignItems: 'center',
              }}>
              <View style={{
                width: 300,
                height: 200,
                justifyContent: 'center', 
                alignItems: 'center',
                backgroundColor: 'white'
              }}>
                <View style={{ 
                  flex: 2, 
                  //backgroundColor: 'red',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 10
                }}>
                  <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignContent: 'space-between' }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Text style={{ color: 'black', fontSize: 18 }}>
                        Nombre:
                      </Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Text>
                        {this.state.nombre}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flex: 1,  flexDirection: 'row', justifyContent: 'space-between', alignContent: 'space-between' }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Text style={{ color: 'black', fontSize: 18 }}>
                        Monto:
                      </Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Text>
                        {this.state.monto} Bs
                      </Text>
                    </View>
                  </View>
                  <View style={{ flex: 1,  flexDirection: 'row', justifyContent: 'space-between', alignContent: 'space-between' }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Text style={{ color: 'black', fontSize: 18 }}>
                        Fecha:
                      </Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Text>
                        {this.state.fecha}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={{ 
                  flex: 1, 
                  flexDirection: 'row',
                  alignItems: 'flex-end'
                }}>
                  <TouchableHighlight
                    onPress={() => {
                      this.setState({ modalVisible: false })
                    }}>
                    <Text style={{ color: 'black', fontSize: 18, marginBottom: 5 }}>Aceptar</Text>
                  </TouchableHighlight>
                </View>
                

              </View>
            </View>
          </Modal>
          <View style={{ flex: 1, alignItems: 'center' }}>
            <Text style={{ 
              fontSize: 20, 
              fontWeight: 'bold', 
              color: 'black',
              marginTop: 10
            }}>
              Pagos Realizados
            </Text>
          </View>
          <View style={{ flex: 9, alignItems: 'center' }}>
            <View style={{ 
                    height: 30, 
                    flexDirection: 'row', 
                    border: 'black 2px solid;', 
                    borderBottomColor: 'gray', 
                    borderBottomWidth: 1,
                    marginLeft: 10,
                    marginRight: 10
                  }}>
                <View style={{ 
                      flex: 2, 
                      alignItems: 'center', 
                      justifyContent: 'center',
                      height: 30
                    }}>
                      <Text style={{ 
                        color: 'black',
                        fontSize: 18,
                     }}>Nombre</Text>
                </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ 
                    color: 'black',
                    fontSize: 18,
                  }}>Monto</Text>
                </View>
            </View>
            <View style={{ height: 20 }}>

            </View>
            {
              this.state.pagos.map((item, key) => {
                return (
                    <View style={{ 
                      height: 50, 
                      flexDirection: 'row', 
                      marginLeft: 10,
                      marginRight: 10
                    }}>
                      
                      <View style={{ 
                        flex: 2, 
                        alignItems: 'center', 
                        justifyContent: 'center',
                        height: 30
                      }}>
                        <TouchableHighlight
                          //style={styles.button}
                          onPress={() => {

                            let fullDate = '';
                            if (item.Fecha != null) {
                              let date = item.Fecha.substring(0, 10);
                              let arr = date.split('-');
                              let y = arr[0];
                              let m = arr[1];
                              let d = arr[2];
                              date = d + '/' + m + '/' + y;
                              let time = item.Fecha.substring(11, 16);
                              fullDate = date + ' ' + time;
                            } 
                            
                            this.setState({ 
                              modalVisible: true,
                              nombre: item.Nombre + ' ' + item.Apellido,
                              monto: item.Monto,
                              fecha: fullDate
                            })
                          }}
                          >
                            <Text style={{ 
                                //color: 'bla',
                                fontSize: 18,
                            }}>{item.Nombre + ' ' + item.Apellido}</Text>
                          </TouchableHighlight>
                      </View>
                      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ 
                          color: 'green',
                          fontSize: 18,
                        }}>{item.Monto} Bs.</Text>
                      </View>
                    </View>
                )
              })
            }
          </View>
        </View> 
      );
    };
}

/*
<View style={styles.container}>
          <Card>
            <Image
              style={styles.cardImage}
              source={{
                uri:
                  "https://www.agiliacenter.com/wp-content/uploads/2018/02/1_bUtWGJQv8QFXTkLF_WBINA-1200x440.png"
              }}
            />
            <Divider
              style={{
                backgroundColor: "blue",
                marginBottom: 10,
                marginTop: 10
              }}
            />
            <Button
              title="Pagar Alquiler"
              type="outline"
              onPress={() => this.refs.modal4.open()}
              style={styles.btn}
            />
          </Card>
          <Modal style={[styles.modal4]} position={"bottom"} ref={"modal4"}>
            <View style={styles.container}>
              <Text style={styles.bntext}>
                El alquiler de este mes es de: 600 bs
              </Text>
            </View>
            <View style={styles.row}>
              <Button
                title="Cancelar"
                buttonStyle={styles.btn}
                onPress={() => this.refs.modal4.close()}
              />
              <Button
                title="Pagar"
                buttonStyle={styles.btn}
                onPress={this._showAlert}
              />
            </View>
          </Modal>
        </View> */

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bntext:{
    paddingTop:10,
    paddingLeft:10,
    fontSize:20,
    fontFamily: 'Clone',
  },
  row: {
    alignContent: "flex-end",
    justifyContent: "flex-end",
    flexDirection: "row"
  },
  cardImage: {
    width: "100%",
    height: 200,
    resizeMode: "cover"
  },
  cardText: {
    padding: 10,
    fontSize: 15
  },
  btn: {
    margin: 10,
    backgroundColor: "#3B5998",
    color: "white",
    padding: 10
  },
  modal: {
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },
  modal4: {
    height: 150
  }
});