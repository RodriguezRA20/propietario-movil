import React from "react";
import { Text, View } from "react-native";
import { createAppContainer } from "react-navigation";
import { createBottomTabNavigator } from "react-navigation-tabs";
//import CasasUbicaciones from './Menu';
import contratoStack from "../Contrato";
import RealizarPago from "./RealizarPagos";
import Mapa from "./Mapa";
import Profile from "./Perfil/Perfil.component";
import ProfileScreen from "./Perfil/Profile1";

const ProcesoTab = createBottomTabNavigator(
  {
    mapa: Mapa,
    Contratos: contratoStack,
    Historial: RealizarPago,
    Perfil: Profile
  },
  {
    tabBarPosition: "top",
    swipeEnabled: true,
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: "#1B5E20",
      inactiveTintColor: "gray"
    },
    style: {
      backgroundColor: "#633689"
    },
    labelStyle: {
      textAlign: "center",
      fontSize: 25
    }
  }
);

export default createAppContainer(ProcesoTab);
