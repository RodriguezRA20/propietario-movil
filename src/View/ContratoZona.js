import React, {Fragment, Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  AsyncStorage,
  TouchableHighlight
} from "react-native";

import t from 'tcomb-form-native';
import ImageFactory from 'react-native-image-picker-form';

  const Form = t.form.Form
  
  
  const DocumentFormStruct = t.struct({
    monto: t.Number,//requiere de nombre Propietario del Inmueble
    habitaciones:t.Number,
    fechapostulacion:t.Date,
    image: t.String
  });
  var _ = require('lodash');
  const parentStyle = _.cloneDeep(t.form.Form.stylesheet);
  const childStyle = _.cloneDeep(t.form.Form.stylesheet);
  parentStyle.fieldset.flex = 1;
  parentStyle.fieldset.flexDirection = 'row';
  
  const formStyles = {
    ...Form.stylesheet,
    formGroup: {
      normal: {
       justifyContent: 'center',
      marginTop: 5,
      padding: 5,
         flexDirection: 'column',
      },
    },
    controlLabel: {
      normal: {
        color: 'green',
        fontSize: 18,
        marginBottom: 10,
        fontWeight: '600',
      },
      // the style applied when a validation error occours
      error: {
        color: 'red',
        fontSize: 18,
        marginBottom: 7,
        fontWeight: '600',
      },
    },
  };
  
  const opt = {
    fields: {
      monto : {
        placeholder: 'Monto',
      },
      habitaciones : {
        placeholder: 'Habitaciones',
      },
      image: {
        config: {
          title: 'Seleccionar Imagen',
          options: ['Abrir Camara', 'Seleccionar de la Galeria', 'Cancel'],
          // Used on Android to style BottomSheet
          style: {
            titleFontFamily: 'Roboto',
                padding: 10,
          }
        },
        error: 'No image provided',
        factory: ImageFactory
      },
      fechapostulacion : {
        label: 'fecha de postulacion',
        format:'YYYY-MM-DD',
        mode:'date',  // muestra el campo Date como DatePickerAndroid 
       dialogMode: 'date'//modo de dialogo 
      },	
      
    
    }
  }
  export default class ContratoZona extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        value: {},
        options: {
          fields: {
            image: {
              config: {
                title: 'Seleccionar Imagen',
                options: ['Abrir Camara', 'Seleccionar de la Galeria', 'Cancel'],
                // Used on Android to style BottomSheet
                style: {
                  titleFontFamily: 'Roboto',
                      padding: 10,
                }
              },
              error: 'No image provided',
              factory: ImageFactory
            },
            fechapostulacion : {
              label: 'fecha de postulacion',
              mode:'date',  // muestra el campo Date como DatePickerAndroid 
             dialogMode: 'date'//modo de dialogo 
            },	
            
          
          },
          //aca personalizamos los estilos de el formulario
            stylesheet:formStyles
        }
      }
    }

    onClick(){
      // console.warn("FOMR ==> ", this.refs.form.getValue());
      var value = this.refs.form.getValue();
      if (value) { // if validation fails, value will be null
        console.log("datos  de getvalue ===> ", value) // value here is an instance of Person   
      }
      
  
      fetch("http://findyourhousesw1.000webhostapp.com/WS/registrar_postulacion.php", {
      //fetch("http://192.168.0.11:8080/FindYourHouse/WS/registrar_postulacion.php", {
        method: "POST",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          monto: value.monto,
          habitaciones: value.habitaciones,
          fechapostulacion:'2019-12-13',//value.fechapostulacion,
          image: value.image
        })
      })
      .then((response) => response.json())
      .then((resp) => {
        console.warn("RESP ==> ", resp);
        if (resp.ok) {
          alert("Registro Satisfactorio");
          this.props.navigation.navigate('usuario', { id: resp.id });//
        } else {
          console.log("LLEGO ==> ", resp.ok); 
          alert( "Error Ocurrio un problema al registrar" +  JSON.stringify({
            monto: value.monto,
            habitaciones: value.habitaciones,
            fechapostulacion:'2019-12-13',//value.fechapostulacion,
            image: value.image
          }));
        }
      })
      .catch((error) => {
        console.log("ERROR ==> ", error);
        alert("Error no se pudo conectar con el servidor");
      })
      
      
      
     // this.props.navigation.navigate('usuario');
        
    }
    componentDidMount() {
        //console.log("============ ", this.props);
    }
  
    render() {
        return (
        <View style={styles.container}>
            {/* display */}
            <Form
            ref="form"        
            type={DocumentFormStruct}
            value={this.state.value}
            //options={this.state.options}
            options={opt}
            />
            
            <TouchableHighlight style={styles.button} onPress={this.onClick.bind(this)} underlayColor='#99d9f4' navigation={this.props.navigation}>
                <Text style={styles.buttonText}>Siguiente</Text>
            </TouchableHighlight>
        </View>
  
        )
    }
}
  var styles = StyleSheet.create({
    container: {
      flexGrow:1,
      justifyContent: 'center',
      marginTop: 5,
      padding: 10,
      flexDirection: 'column',
      backgroundColor: '#ffffff',
    },
    estiloFormulario:{
              flex:1,
            flexDirection: 'column',
        
    },
    buttonText: {
      fontSize: 18,
      color: 'white',
      alignSelf: 'center'
    },
    button: {
      height: 36,
      backgroundColor: '#003300',
      borderColor: '#003300',
      borderWidth: 1,
      borderRadius: 8,
      marginBottom: 20,
      padding: 10,
      alignSelf: 'stretch',
      justifyContent: 'center'
    }
  });
