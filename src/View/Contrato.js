import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import  ContratoZona from './ContratoZona';
import ContratoCliente from './ContratoCliente';
import Mapa from './componentes/Mapa';
import Menu from './componentes/Menu';
import ProcesoTab from './componentes/ProcesoTab';
 

   const contratoStack = createStackNavigator(
     {
       zona: {
         screen: ContratoZona,
         navigationOptions: {
           header: null
         }
       },
       usuario: {
         screen: ContratoCliente,
         navigationOptions: {
           header: null
         }
       },
     },
     {
       initialRouteName: "zona"
     }
   );
   const Contrato=createAppContainer(contratoStack);
   export default Contrato




 