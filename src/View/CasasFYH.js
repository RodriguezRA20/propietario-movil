import React, {Component} from 'react';
import { StyleSheet,TextInput,Text, View,TouchableOpacity,StatusBar} from 'react-native';
import {Content,Header,Left,Right,Icon} from 'native-base'


export default class CasasFYH extends Component{
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password:'',
        };
    }

    lenvantarDrawer=(props)=>{
      // console.warn(this.navigation)
        this.props.navigation.openDrawer();
     }
    Acceso=(props)=>{
    //   console.warn(this.props) con esto puedo mostrar en pantalla lo que deseo mostrar
    //Aca diseñaremos el login que hara la autentificacion con firebase
    
    
    this.props.navigation.navigate('Registrate');
      }
  render() {
    return (
      <View style={styles.container}> 
	        <StatusBar
        backgroundColor='#003300' barStyle='light-content'/>
        <TextInput 
        style={styles.inputBox} 
        placeholder="Email" 
        keyboardType="email-address" 
        placeholderTextColor="#ffffff"
        onSubmitEditing={()=>this.password.focus()}
        />
        <TextInput 
        style={styles.inputBox} 
        placeholder="Password" 
        secureTextEntry={true} 
        placeholderTextColor="#ffffff"
        ref={(input)=>this.password=input}
        />
        <TouchableOpacity style={styles.Button} onPress={this.Acceso} navigation={this.props.navigation}>
            <Text style={styles.ButtonText}>{this.props.type}CASAS FIND YOUR HOUSE</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#1B5E20',
    },
    inputBox:{
        width:300,
        backgroundColor:'rgba(255,255,255,0.3)',
        borderRadius:25,
        paddingHorizontal:16,
        fontSize:16,
        color:'#ffffff',
        marginVertical:10,
    },
    Button:{
        width:300,
        backgroundColor:'#003300',
        borderRadius:25,
        marginVertical:10,
        paddingVertical:13,
    },
    ButtonText:{
        fontSize:16,
        fontWeight:'500',
        color:'#ffffff',
        textAlign:'center',
    }
  });