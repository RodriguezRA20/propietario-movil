import React, { Component } from 'react';
import { View, StyleSheet, Text, Alert,  } from "react-native";

/*
* Controlador de las Acciones que se realizan en las vistas
*/
import Reflux from 'reflux'

let AccionLogin=Reflux.createAction([
    'IngresoCorrecto'
]);
export default AccionLogin; 
