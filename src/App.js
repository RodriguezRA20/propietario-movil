
import 'react-native-gesture-handler';

import Login from './View/Login';

import ProcesoTab from './View/componentes/ProcesoTab';
import Registro from './View/componentes/Registro';
import ContratoCliente from '../src/View/ContratoCliente'
import Contrato from './View/Contrato'; 

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Mapa from './View/componentes/Mapa';

const MainNavigator = createStackNavigator(
  {
    Home: {
      screen: Login, //Login ContratoCliente
      navigationOptions: {
        header: null
      }
    },
    Registrate: {
      screen: Registro,
      navigationOptions: {
        header: null
      }
    },
    Menu: {
      screen: ProcesoTab,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: "Home"
  }
);

const App = createAppContainer(MainNavigator);

export default App;





